# logger

It is simple logger with fields and levels.

It usage js console.log and type safe fo syscall/js.

## Example

```go
log := logger.New()

log = log.WithString("key", "value")

log.WithError(nil).Warn("Some warning")

log.Info("Info text")
```

### With types

**Go code**

```go
log := logger.New()
log.WithInt64("i64", 64).
	WithInt64s("i64s", []int64{1, 3}).
	WithFloat64s("f64s", []float64{1.45, 1}).
	WithInterface("interface", log).
	WithInterfaces("interfaces_wrong", []interface{}{log, 222222}).
	WithInterfaces("interfaces_ok", []interface{}{3, 4, 5}).
	WithStrings("strings", []string{"ddd", "1"}).
	WithError(nil).
	WithErr("not_nil_error", errors.New("some err")).
	Debug("Fields")
	
// Do not use this way. It is very expensive. Use l.WithFields()
```

**JS console**

```js
{
	fields: {
		i64: 64
		i64s: Array [ 1, 3 ]
		f64s: Array [ 1.45, 1 ]
		interface: "cannot convert this val in WithInterface method"
		interfaces_wrong: "cannot convert this val in WithInterfaces method"
		interfaces_ok: Array(3) [ 3, 4, 5 ]
		strings: Array [ "ddd", "1" ]
		error: null
		not_nil_error: "some err"
	}
	msg: "Fields"
}
```

### Without types

If you need more fat logger then use power wrapper

**Go code**

```go
log := power.New(logger.New())

log.WithField("any_type", log).
	WithField("my_field", 5).
	Debug("Any types fields")
```

**JS console**

```js
{
	fields: {
		any_type: "&logger.Logger{fields:logger.fields{}}"
		my_field: 5
	}
	msg: "Any types fields"
}
```
