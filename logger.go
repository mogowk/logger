// +build go1.12,wasm

// Package logger represents console logger with levels
// used typed fields
package logger

import (
	"gitlab.com/mogowk/wrap/console"
)

type fields map[string]interface{}

// Logger main type of the logger
type Logger struct {
	fields fields
}

// New creates New logger
func New() *Logger {
	return &Logger{
		fields: make(fields),
	}
}

func copyFields(src, dst fields) {
	for key, value := range src {
		dst[key] = value
	}
}

func (l *Logger) withField(key string, value interface{}) *Logger {
	newLogger := &Logger{
		fields: make(map[string]interface{}, len(l.fields)+1),
	}
	copyFields(l.fields, newLogger.fields)
	newLogger.fields[key] = value

	return newLogger
}

// WithFields appends fields set to list
// use only base types: nil, bool, integers, floats, string
func (l *Logger) WithFields(f map[string]interface{}) *Logger {
	newLogger := &Logger{
		fields: make(map[string]interface{}, len(l.fields)+len(f)),
	}
	copyFields(l.fields, newLogger.fields)
	copyFields(f, newLogger.fields)

	return newLogger
}

func (l *Logger) makeMsg(f fields, msg string) map[string]interface{} {
	if len(f) == 0 {
		return map[string]interface{}{"msg": msg}
	}
	return map[string]interface{}{"fields": map[string]interface{}(f), "msg": msg}
}

// Debug prints debug message
func (l *Logger) Debug(msg string) {
	console.Debug(l.makeMsg(l.fields, msg))
}

// Info prints info message
func (l *Logger) Info(msg string) {
	console.Info(l.makeMsg(l.fields, msg))
}

// Warn prints warning message
func (l *Logger) Warn(msg string) {
	console.Warn(l.makeMsg(l.fields, msg))
}

// Error prints error message with stacktrace
func (l *Logger) Error(msg string) {
	console.Error(l.makeMsg(l.fields, msg))
}

// Trace prints stacktrace
func (l *Logger) Trace() {
	console.Trace()
}
