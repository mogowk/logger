// +build go1.12,wasm

package power

import (
	"fmt"

	"gitlab.com/mogowk/wrap/valueof"
)

// WithValue appends any typed param to field list in Go-syntax representation (%#v) fmt format
func (l Logger) WithValue(key string, value interface{}) Logger {
	return Logger{l.log.WithString(key, fmt.Sprintf("%#v", value))}
}

// WithField tries to convert value to JS type and if it not passible write it like a WithValue
func (l Logger) WithField(key string, value interface{}) Logger {
	jsval, fatal := valueof.ValueOf(value)
	if fatal {
		return l.WithValue(key, value)
	}
	return Logger{l.log.WithJSValue(key, jsval)}
}

// WithErr append error to field list with custom key
func (l Logger) WithErr(key string, err error) Logger {
	return Logger{l.log.WithErr(key, err)}
}

// WithError append error to field list with "error" key
func (l Logger) WithError(err error) Logger {
	return Logger{l.log.WithError(err)}
}

// WithFields appends fields set to list
// use only base types: nil, bool, integers, floats, string
func (l Logger) WithFields(fields map[string]interface{}) Logger {
	return Logger{l.log.WithFields(fields)}
}
