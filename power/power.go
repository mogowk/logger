// +build go1.12,wasm

// Package power is a more powered logger but with fmt (fat)
package power

import (
	"fmt"

	"gitlab.com/mogowk/logger"
)

// Logger fat wrapper of simple logger
type Logger struct {
	log *logger.Logger
}

// New creates new enriched logger
func New(l *logger.Logger) Logger {
	return Logger{log: l}
}

// Debug prints message with debug level
func (l Logger) Debug(msg string) {
	l.log.Debug(msg)
}

// Debugf prints formatted message with debug level
func (l Logger) Debugf(format string, args ...interface{}) {
	l.Debug(fmt.Sprintf(format, args...))
}

// Info prints message with info level
func (l Logger) Info(msg string) {
	l.log.Info(msg)
}

// Infof prints formatted message with info level
func (l Logger) Infof(format string, args ...interface{}) {
	l.Info(fmt.Sprintf(format, args...))
}

// Warn prints message with warning level
func (l Logger) Warn(msg string) {
	l.log.Warn(msg)
}

// Warnf prints formatted message with warning level
func (l Logger) Warnf(format string, args ...interface{}) {
	l.Warn(fmt.Sprintf(format, args...))
}

// Error prints message with error level
func (l Logger) Error(msg string) {
	l.log.Error(msg)
}

// Errorf prints formatted message with error level
func (l Logger) Errorf(format string, args ...interface{}) {
	l.Error(fmt.Sprintf(format, args...))
}

// Trace prints stacktrace
func (l Logger) Trace() {
	l.log.Trace()
}
