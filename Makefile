.PHONY: lints

lints:
	GOOS=js GOARCH=wasm golangci-lint run -c ./.linters.yml