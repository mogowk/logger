// +build go1.12,wasm

package logger

import (
	"syscall/js"
	"unsafe"

	"gitlab.com/mogowk/wrap/valueof"
)

// syscall/js cannot convert any type in interface
// only types in syscall/js/js.go in ValueOf() method

// WithBool appends bool to field list
func (l *Logger) WithBool(key string, val bool) *Logger {
	return l.withField(key, val)
}

// int

// WithInt appends int to field list
func (l *Logger) WithInt(key string, val int) *Logger {
	return l.withField(key, val)
}

// WithInt8 appends int8 to field list
func (l *Logger) WithInt8(key string, val int8) *Logger {
	return l.withField(key, val)
}

// WithInt16 appends int16 to field list
func (l *Logger) WithInt16(key string, val int16) *Logger {
	return l.withField(key, val)
}

// WithInt32 appends int32 to field list
func (l *Logger) WithInt32(key string, val int32) *Logger {
	return l.withField(key, val)
}

// WithInt64 appends int64 to field list
func (l *Logger) WithInt64(key string, val int64) *Logger {
	return l.withField(key, val)
}

// WithInts appends []int to field list
func (l *Logger) WithInts(key string, val []int) *Logger {
	slice := make([]interface{}, len(val))
	for i, v := range val {
		slice[i] = v
	}
	return l.withField(key, slice)
}

// WithInt8s appends []int8 to field list
func (l *Logger) WithInt8s(key string, val []int8) *Logger {
	slice := make([]interface{}, len(val))
	for i, v := range val {
		slice[i] = v
	}
	return l.withField(key, slice)
}

// WithInt16s appends []int16 to field list
func (l *Logger) WithInt16s(key string, val []int16) *Logger {
	slice := make([]interface{}, len(val))
	for i, v := range val {
		slice[i] = v
	}
	return l.withField(key, slice)
}

// WithInt32s appends []int32 to field list
func (l *Logger) WithInt32s(key string, val []int32) *Logger {
	slice := make([]interface{}, len(val))
	for i, v := range val {
		slice[i] = v
	}
	return l.withField(key, slice)
}

// WithInt64s appends []int64 to field list
func (l *Logger) WithInt64s(key string, val []int64) *Logger {
	slice := make([]interface{}, len(val))
	for i, v := range val {
		slice[i] = v
	}
	return l.withField(key, slice)
}

// uint

// WithUint appends uint to field list
func (l *Logger) WithUint(key string, val uint) *Logger {
	return l.withField(key, val)
}

// WithUint8 appends uint8 to field list
func (l *Logger) WithUint8(key string, val uint8) *Logger {
	return l.withField(key, val)
}

// WithUint16 appends uint16 to field list
func (l *Logger) WithUint16(key string, val uint16) *Logger {
	return l.withField(key, val)
}

// WithUint32 appends uint32 to field list
func (l *Logger) WithUint32(key string, val uint32) *Logger {
	return l.withField(key, val)
}

// WithUint64 appends uint64 to field list
func (l *Logger) WithUint64(key string, val uint64) *Logger {
	return l.withField(key, val)
}

// WithUints appends []uint to field list
func (l *Logger) WithUints(key string, val []uint) *Logger {
	slice := make([]interface{}, len(val))
	for i, v := range val {
		slice[i] = v
	}
	return l.withField(key, slice)
}

// WithUint8s appends []uint8 to field list
func (l *Logger) WithUint8s(key string, val []uint8) *Logger {
	slice := make([]interface{}, len(val))
	for i, v := range val {
		slice[i] = v
	}
	return l.withField(key, slice)
}

// WithUint16s appends []uint16 to field list
func (l *Logger) WithUint16s(key string, val []uint16) *Logger {
	slice := make([]interface{}, len(val))
	for i, v := range val {
		slice[i] = v
	}
	return l.withField(key, slice)
}

// WithUint32s appends []uint32 to field list
func (l *Logger) WithUint32s(key string, val []uint32) *Logger {
	slice := make([]interface{}, len(val))
	for i, v := range val {
		slice[i] = v
	}
	return l.withField(key, slice)
}

// WithUint64s appends []uint64 to field list
func (l *Logger) WithUint64s(key string, val []uint64) *Logger {
	slice := make([]interface{}, len(val))
	for i, v := range val {
		slice[i] = v
	}
	return l.withField(key, slice)
}

// WithUintptr appends uintptr to field list
func (l *Logger) WithUintptr(key string, val uintptr) *Logger {
	return l.withField(key, val)
}

// WithUnsafePointer appends unsafe.Pointer to field list
func (l *Logger) WithUnsafePointer(key string, val unsafe.Pointer) *Logger {
	return l.withField(key, val)
}

// float

// WithFloat32 appends float32 to field list
func (l *Logger) WithFloat32(key string, val float32) *Logger {
	return l.withField(key, val)
}

// WithFloat64 appends float64 to field list
func (l *Logger) WithFloat64(key string, val float64) *Logger {
	return l.withField(key, val)
}

// WithFloat32s appends []float32 to field list
func (l *Logger) WithFloat32s(key string, val []float32) *Logger {
	slice := make([]interface{}, len(val))
	for i, v := range val {
		slice[i] = v
	}
	return l.withField(key, slice)
}

// WithFloat64s appends []float64 to field list
func (l *Logger) WithFloat64s(key string, val []float64) *Logger {
	slice := make([]interface{}, len(val))
	for i, v := range val {
		slice[i] = v
	}
	return l.withField(key, slice)
}

// string

// WithString appends string to field list
func (l *Logger) WithString(key, val string) *Logger {
	return l.withField(key, val)
}

// WithStrings appends []string to field list
func (l *Logger) WithStrings(key string, val []string) *Logger {
	slice := make([]interface{}, len(val))
	for i, v := range val {
		slice[i] = v
	}
	return l.withField(key, slice)
}

// Other

// WithJSValue appends js.Value to field list
func (l *Logger) WithJSValue(key string, val js.Value) *Logger {
	return l.withField(key, val)
}

// WithJSWrapper appends js.Wrapper to field list
func (l *Logger) WithJSWrapper(key string, val js.Wrapper) *Logger {
	return l.withField(key, val)
}

// WithInterface appends interface{} to field list
// make sure that val contains type of this file only
func (l *Logger) WithInterface(key string, val interface{}) *Logger {
	var realValue interface{}
	jsValue, fatal := valueof.ValueOf(val)
	if fatal {
		realValue = "cannot convert this val in WithInterface method"
	} else {
		realValue = jsValue
	}

	return l.withField(key, realValue)
}

// WithInterfaces appends []interface{} to field list
// make sure that slice contains types of this file only
func (l *Logger) WithInterfaces(key string, val []interface{}) *Logger {
	var realValue interface{}
	jsValue, fatal := valueof.ValueOf(val)
	if fatal {
		realValue = "cannot convert this val in WithInterfaces method"
	} else {
		realValue = jsValue
	}

	return l.withField(key, realValue)
}

// WithObject appends map[string]interface{} to field list
// make sure that interface contains types of this file only
func (l *Logger) WithObject(key string, val map[string]interface{}) *Logger {
	var realValue interface{}
	jsValue, fatal := valueof.ValueOf(val)
	if fatal {
		realValue = "cannot convert this val in WithObject method"
	} else {
		realValue = jsValue
	}

	return l.withField(key, realValue)
}

// WithErr appends error to field list with custom key
func (l *Logger) WithErr(key string, err error) *Logger {
	if err == nil {
		return l.withField(key, nil)
	}
	return l.withField(key, err.Error())
}

// WithError appends error to field list with "error" key
func (l *Logger) WithError(err error) *Logger {
	return l.WithErr("error", err)
}
